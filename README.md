Aplikasi android untuk Monitoring Curah Hujan

## Deployment

APK dapat dideploy langsung ke perangkat android atau pada emulator android. Sistem operasi Android minimal Versi 5.0 Lollipop atau API level 21.

## Built With

* [Android Studio](https://developer.android.com/studio) - IDE untuk pengembangan aplikasi & dependency manager
* [Firebase](https://firebase.google.com/) - Backend

## Authors

* **Ferian Chandra** - *Initial work* - [Profile](https://gitlab.com/talkactive4)

See also the list of contributors who participated in this project.

## License

This project is licensed under the MIT License - see the LICENSE.md file for details

## Acknowledgments

* [Readme.md template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
