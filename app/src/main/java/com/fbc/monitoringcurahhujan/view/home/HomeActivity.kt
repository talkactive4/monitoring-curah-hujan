package com.fbc.monitoringcurahhujan.view.home

import android.content.Intent
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.fbc.monitoringcurahhujan.R
import com.fbc.monitoringcurahhujan.data.CurahHujan
import com.fbc.monitoringcurahhujan.databinding.ActivityHomeBinding
import com.fbc.monitoringcurahhujan.view.search.SearchActivity
import com.fbc.monitoringcurahhujan.view.settings.SettingsActivity
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.utils.Utils
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import android.util.Log

class HomeActivity : AppCompatActivity() {

    private val TAG = "HomeActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // View model
        val viewModel = HomeViewModel()
        // Observers
        viewModel.latestData.observe(this, Observer {
            it?.let{
                viewModel.onDataChanged(it)
            }
        })
        viewModel.onShowLoading.observe(this, Observer {
            it?.let { visibility ->
                binding.loadingView.tvLoadingText.visibility = visibility
                binding.loadingView.pbLoadingProgress.visibility = visibility
            }
        })
        viewModel.contentVisible.observe(this, Observer {
            it?.let {visibility ->
                binding.rainfallView.visibility = visibility
            }
        })
        viewModel.showMessage.observe(this, Observer {
            it?.let{
                Toast.makeText(this,it, Toast.LENGTH_SHORT).show()
            }
        })
        viewModel.rainfallData.observe(this, Observer {
            if (it == null){
                binding.rainfallView.displayedChild = 1
            }
            it?.let{curahHujan ->
                val sdf = SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm:ss", Locale.getDefault())
                binding.latestData.tvDate.text = sdf.format(curahHujan.waktu.toDate())
                val rainfallData = curahHujan.curahHujan.toString() + " mm"
                binding.latestData.tvRainfall.text = rainfallData
                binding.latestData.tvStatus.text = curahHujan.statusHujan
                binding.rainfallView.displayedChild = 0
            }
        })

        // line chart setup
        binding.chart1.description.text = "Moisturity per time"
        binding.chart1.setTouchEnabled(true)
        binding.chart1.isDragEnabled = true
        binding.chart1.setPinchZoom(true)

        val xAxis:XAxis
        xAxis = binding.chart1.xAxis
        xAxis.enableGridDashedLine(10f,10f,0f)

        val yAxis:YAxis
        yAxis = binding.chart1.axisLeft
        binding.chart1.axisRight.isEnabled = false
        yAxis.enableGridDashedLine(10f,10f,0f)
        //yAxis.axisMaximum = 110f
        //yAxis.axisMinimum = -10f
        binding.chart1.animateX(1000)

        val l: Legend = binding.chart1.legend
        l.setForm(Legend.LegendForm.LINE)

        viewModel.rainfallDatas.observe(this,Observer{list->
            if(list.isNullOrEmpty()){
                binding.chart1.visibility = View.GONE
            } else{
                binding.chart1.visibility = View.VISIBLE
                binding.chart1.description.isEnabled = true

                setData(binding.chart1,list)
            }
        })

        binding.latestDataEmpty.btnRetry.setOnClickListener {
            Toast.makeText(this,"Retrying...",Toast.LENGTH_SHORT).show()
        }
    }

    private fun setData(chart: LineChart, list:List<CurahHujan>){
        val values = ArrayList<Entry>()
        val timestamps = ArrayList<Long>()
        val datas = ArrayList<Pair<String,Double>>()
        for(i in (list.size - 1) downTo 0){
            timestamps.add(list[i].waktu.toDate().time)
            datas.add(Pair(list[i].curahHujan.toFloat().toString(),list[i].curahHujan))
            values.add(Entry((list.size - i).toFloat(),list[i].curahHujan.toFloat()))
        }

        val xAxis = chart.xAxis
        xAxis.valueFormatter = CustomAxisValueFormatter(timestamps)

        val yAxis = chart.axisLeft
        yAxis.valueFormatter = CustomYAxisValueFormatter(datas)

        val set1:LineDataSet
        if(chart.data != null && chart.data.dataSetCount > 0){
            set1 = chart.data.getDataSetByIndex(0) as LineDataSet
            set1.values = values
            set1.notifyDataSetChanged()
            chart.data.notifyDataChanged()
            chart.notifyDataSetChanged()
        } else {
            set1 = LineDataSet(values, "Data Curah Hujan")
            set1.setDrawIcons(false)
            set1.enableDashedLine(10f,5f,0f)

            set1.color = Color.BLACK
            set1.setCircleColor(Color.BLACK)

            set1.lineWidth = 1f
            set1.circleRadius = 3f

            set1.setDrawCircleHole(false)

            set1.formLineWidth = 1f
            set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f,5f),0f)
            set1.formSize = 15f

            set1.valueTextSize = 9f
            set1.enableDashedLine(10f,5f,0f)

            set1.setDrawFilled(true)
            set1.setFillFormatter { dataSet, dataProvider ->
                chart.axisLeft.axisMinimum
            }

            if(Utils.getSDKInt() >= 18){
                val drawable: Drawable? = ContextCompat.getDrawable(this,R.drawable.fade_red)
                drawable?.let { set1.fillDrawable = drawable }
            } else{
                set1.fillColor = Color.BLACK
            }

            val dataSets = ArrayList<ILineDataSet>()
            dataSets.add(set1)

            val data = LineData(dataSets)

            chart.data = data
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menu_search -> {
                startActivity(Intent(this,SearchActivity::class.java))
                return true
            }
            R.id.menu_settings ->{
                startActivity(Intent(this, SettingsActivity::class.java))
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private class CustomAxisValueFormatter(private val timestamps:List<Long>): ValueFormatter() {
        override fun getFormattedValue(value: Float): String {
            val date = Date(timestamps[value.toInt() - 1])
            val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
            return sdf.format(date)
        }
    }

    private class CustomYAxisValueFormatter(private val curahHujan:List<Pair<String,Double>>): ValueFormatter() {
        override fun getFormattedValue(value: Float): String {
            var s:String = "0 mm"
            for(i in curahHujan){
                if(i.first == value.toString()){
                    s = i.second.toString() + " mm"
                }
            }
            
            return s
        }
    }

}