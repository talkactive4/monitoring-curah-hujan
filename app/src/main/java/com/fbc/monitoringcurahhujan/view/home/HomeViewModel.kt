package com.fbc.monitoringcurahhujan.view.home

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.fbc.monitoringcurahhujan.data.CurahHujan
import com.fbc.monitoringcurahhujan.network.repository.CurahHujanRepository
import com.google.firebase.firestore.QuerySnapshot

class HomeViewModel : ViewModel()  {

    private val repo = CurahHujanRepository()

    val latestData = repo.latestRainfalls

    private val _rainfallData = MutableLiveData<CurahHujan?>()
    val rainfallData:LiveData<CurahHujan?>
        get() = _rainfallData

    private val _rainfallDatas = MutableLiveData<List<CurahHujan>?>()
    val rainfallDatas: LiveData<List<CurahHujan>?>
        get() = _rainfallDatas

    private val _showLoading = MutableLiveData<Boolean>()
    val onShowLoading = Transformations.map(_showLoading){
        if (it){
            View.VISIBLE
        } else {
            View.GONE
        }
    }
    val contentVisible = Transformations.map(onShowLoading){
        when (it) {
            View.VISIBLE -> View.GONE
            else -> View.VISIBLE
        }
    }

    private val _message = MutableLiveData<String>()
    val showMessage = Transformations.map(_message){
        when (it){
            null -> null
            "" -> null
            else -> it
        }
    }

    init {
        _showLoading.value = false
        _rainfallData.value = null
        _rainfallDatas.value = null
        _message.value = null
    }

    fun onDataChanged(snapshot:QuerySnapshot){
        var first = true
        val array = ArrayList<CurahHujan>()
        for(item in snapshot){
            if(first) {
                _rainfallData.value = item.toObject(CurahHujan::class.java)
                first = false
            }
            array.add(item.toObject(CurahHujan::class.java))
        }
        _rainfallDatas.value = array
        _showLoading.value = false
    }

    fun showLoading(b: Boolean){
        _showLoading.value = b
    }

}