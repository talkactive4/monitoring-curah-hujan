package com.fbc.monitoringcurahhujan.view.search

import android.R
import android.app.DatePickerDialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.fbc.monitoringcurahhujan.databinding.ActivitySearchBinding
import com.fbc.monitoringcurahhujan.source.ExportCsvUtils
import java.text.SimpleDateFormat
import java.util.*

class SearchActivity : AppCompatActivity() {

    private val REQUEST_PERMISSION_STORAGE = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.searchToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // View Models
        val viewModel = SearchViewModel()
        // Observers
        viewModel.onShowLoading.observe(this, Observer {
            it?.let { visibility ->
                binding.loadingView.tvLoadingText.visibility = visibility
                binding.loadingView.pbLoadingProgress.visibility = visibility
            }
        })
        viewModel.contentVisible.observe(this, Observer {
            it?.let {visibility ->
                binding.content.visibility = visibility
            }
        })
        viewModel.message.observe(this, Observer {
            it?.let{
                Toast.makeText(this,it,Toast.LENGTH_SHORT).show()
            }
        })

        val adapter = SearchAdapter(SearchAdapter.ItemSearchClickListener{
            // do nothing
        })
        binding.rvSearchResult.adapter = adapter

        viewModel.rainfallData.observe(this, Observer {
            if (it.isNullOrEmpty()){
                binding.content.displayedChild = 1
            }
            it?.let{list ->
                if (!list.isEmpty()){
                    adapter.submitList(list)
                    binding.content.displayedChild = 0
                }
            }
        })

        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())

        binding.tvSearchFrom.setOnClickListener {
            val calendar = Calendar.getInstance()
            val datePickerDialog = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                    val dateString = "$dayOfMonth/${month+1}/$year"
                    binding.tvSearchFrom.text = dateString
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )
            datePickerDialog.show()
        }

        binding.tvSearchTo.setOnClickListener {
            val calendar = Calendar.getInstance()
            val datePickerDialog = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                    val dateString = "$dayOfMonth/${month+1}/$year"
                    binding.tvSearchTo.text = dateString
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )
            datePickerDialog.show()
        }

        binding.btnSearch.setOnClickListener{
            val custom = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
            val from = try{custom.parse(binding.tvSearchFrom.text.toString()+" 00:00:00")}catch(e:Exception){null}
            val to = try{custom.parse(binding.tvSearchTo.text.toString()+" 23:59:59")}catch(e:Exception){null}
            viewModel.retrieveData(from,to)
        }

        binding.btnExport.setOnClickListener {
            // List of necessary permissions
            val neededPermissions: Array<String> = arrayOf(
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            )

            // check permission
            if (arePermissionsGranted(neededPermissions)) {
                val listData = viewModel.getListData()
                if(listData == null || listData.isEmpty()){
                    viewModel.showMessage("Data kosong!")
                } else{
                    val exportUtils = ExportCsvUtils(this, ExportCsvUtils.OnExportCompleteListener({
                        success,message->
                        viewModel.showMessage(message)
                    }))

                    exportUtils.startExport(listData,"rainfall_"+Calendar.getInstance().timeInMillis)
                }

            } else {
                requestPermissionsCompat(neededPermissions, REQUEST_PERMISSION_STORAGE)
            }
        }

    }


    // retrieve result from request permission
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var canExport = false
        for (element in grantResults) {
            if (requestCode == REQUEST_PERMISSION_STORAGE && grantResults.isNotEmpty() &&
                element == PackageManager.PERMISSION_GRANTED
            ) {
                canExport = true
            } else {
                canExport = false
                break
            }
        }
        if (canExport) {
            // do export
        } else {
            Toast.makeText(
                this,
                "Permission dibutuhkan untuk mengekspor data!",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    // check if permissions are granted
    private fun arePermissionsGranted(permissions: Array<String>): Boolean {
        permissions.forEach { permission ->
            if (ContextCompat.checkSelfPermission(
                    this,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    // request permission to user
    private fun requestPermissionsCompat(permissions: Array<String>, requestCode: Int) {
        ActivityCompat.requestPermissions(this, permissions, requestCode)
    }
}