package com.fbc.monitoringcurahhujan.view.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fbc.monitoringcurahhujan.data.CurahHujan
import com.fbc.monitoringcurahhujan.databinding.ItemSearchBinding
import java.text.SimpleDateFormat
import java.util.*

class SearchAdapter(private val clickListener:ItemSearchClickListener)
    : ListAdapter<Pair<String,CurahHujan>, SearchAdapter.ItemSearchViewHolder>(ItemDiffUtils()){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemSearchViewHolder {
        return ItemSearchViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ItemSearchViewHolder, position: Int) {
        holder.bind(getItem(position).second,getItem(position).first,position,clickListener)
    }

    // ViewHolder implementation
    class ItemSearchViewHolder private constructor(private val binding:ItemSearchBinding)
        : RecyclerView.ViewHolder(binding.root){
        companion object{
            fun from(parent: ViewGroup):ItemSearchViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemSearchBinding.inflate(layoutInflater,parent,false)
                return ItemSearchViewHolder(binding)
            }
        }

        fun bind(curahHujan: CurahHujan, key:String, index:Int, clickListener: ItemSearchClickListener){
            val rainfallData = curahHujan.curahHujan.toString() + " mm"
            binding.tvItemRainfall.text =  rainfallData
            binding.tvItemStatus.text = curahHujan.statusHujan
            val sdf = SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm:ss", Locale.getDefault())
            val dateString = sdf.format(curahHujan.waktu.toDate())
            binding.tvItemDate.text = dateString

            binding.root.setOnClickListener{
                clickListener.onClick(key)
            }
        }
    }

    // item click listener
    class ItemSearchClickListener(private val clickListener: (String) -> Unit) {
        fun onClick(key: String) = clickListener(key)
    }

    // Detect difference
    private class ItemDiffUtils : DiffUtil.ItemCallback<Pair<String, CurahHujan>>() {
        override fun areItemsTheSame(oldItem: Pair<String, CurahHujan>, newItem: Pair<String, CurahHujan>): Boolean {
            return oldItem.first == newItem.first
        }

        override fun areContentsTheSame(oldItem: Pair<String, CurahHujan>, newItem: Pair<String, CurahHujan>): Boolean {
            return oldItem.second.statusHujan == newItem.second.statusHujan ||
                    oldItem.second.curahHujan == newItem.second.curahHujan ||
                    oldItem.second.waktu == newItem.second.waktu
        }

    }
}