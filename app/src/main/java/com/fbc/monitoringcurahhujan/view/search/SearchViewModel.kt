package com.fbc.monitoringcurahhujan.view.search

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.fbc.monitoringcurahhujan.data.CurahHujan
import com.fbc.monitoringcurahhujan.network.repository.CurahHujanRepository
import com.google.firebase.firestore.QuerySnapshot
import java.util.*

class SearchViewModel : ViewModel()  {

    private val repo = CurahHujanRepository()

    private val _rainfallData = MutableLiveData<List<Pair<String,CurahHujan>>>()
    val rainfallData: LiveData<List<Pair<String,CurahHujan>>>
        get() = _rainfallData

    private val _showLoading = MutableLiveData<Boolean>()
    val onShowLoading = Transformations.map(_showLoading){
        if (it){
            View.VISIBLE
        } else {
            View.GONE
        }
    }
    val contentVisible = Transformations.map(onShowLoading){
        when (it) {
            View.VISIBLE -> View.GONE
            else -> View.VISIBLE
        }
    }
    private val _message = MutableLiveData<String>()
    val message = Transformations.map(_message){
        when (it){
            null -> null
            "" -> null
            else -> it
        }
    }
    fun showMessage(message:String){
        _message.value= message
    }

    init {
        _showLoading.value = false
        _rainfallData.value = null
        _message.value = null
    }

    private fun onRetrieved(snapshot:QuerySnapshot?){
        if (snapshot != null){
            val arrayRainfall = ArrayList<Pair<String, CurahHujan>>()
            for(item in snapshot){
                val rainfall = item.toObject(CurahHujan::class.java)
                arrayRainfall.add(Pair(item.id,rainfall))
            }
            _rainfallData.value = arrayRainfall.toList()
        } else {
            _rainfallData.value = null
            _message.value = "Data is empty"
        }
        _showLoading.value = false
    }

    fun retrieveData(from:Date?,to:Date?){
        _showLoading.value = true
        if(from != null && to != null){
            repo.getRainfallBetween(from,to).addSnapshotListener { snapshot, firebaseFirestoreException ->
                onRetrieved(snapshot)
            }
        } else if(from !=null && to == null){
            repo.getRainfallAfter(from).addSnapshotListener { snapshot, firebaseFirestoreException ->
                onRetrieved(snapshot)
            }
        } else if(from == null && to != null){
            repo.getRainfallBefore(to).addSnapshotListener { snapshot, firebaseFirestoreException ->
                onRetrieved(snapshot)
            }
        } else{
            repo.getRainfall().addSnapshotListener { snapshot, firebaseFirestoreException ->
                onRetrieved(snapshot)
            }
        }
    }

    fun showLoading(b: Boolean){
        _showLoading.value = b
    }

    fun getListData():List<Pair<String,CurahHujan>>?{
        return _rainfallData.value
    }
}