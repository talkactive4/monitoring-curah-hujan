package com.fbc.monitoringcurahhujan.view.settings

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.fbc.monitoringcurahhujan.R
import com.fbc.monitoringcurahhujan.service.PeriodicalNotificationService
import com.fbc.monitoringcurahhujan.service.PeriodicalNotificationService.Companion.ACTION_START
import com.fbc.monitoringcurahhujan.service.PeriodicalNotificationService.Companion.ACTION_STOP
import com.fbc.monitoringcurahhujan.source.preference.NotificationPreference

class SettingsActivity : AppCompatActivity() {

    private val TAG = "SettingsActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_settings,PrefFragment())
            .commit()
    }

    class PrefFragment : PreferenceFragmentCompat(),
        Preference.OnPreferenceChangeListener {

        private val TAG = "Settings.PrefFragment"

        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.preferences, rootKey)

            val activePref: Preference? =
                findPreference(getString(R.string.key_active_preference))
            bindPreferenceSummaryToValue(activePref)
            val minutesPref: Preference? =
                findPreference(getString(R.string.key_minutes_preference))
            bindPreferenceSummaryToValue(minutesPref)

        }


        override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
            if (preference != null){
                if (preference.key.equals(getString(R.string.key_minutes_preference))){
                    val listPreference = preference as androidx.preference.ListPreference
                    val prefIndex = listPreference.findIndexOfValue(newValue.toString())
                    val mSummary = when(newValue.toString()){
                        "15" -> "15 minutes"
                        "30" -> "30 minutes"
                        "60" -> "1 hour"
                        "120" -> "2 hours"
                        "360" -> "6 hours"
                        "720" -> "12 hours"
                        "1440" -> "1 day"
                        else -> "Tap here to pick intervals"
                    }
                    if (prefIndex >=0){
                        listPreference.setSummary(mSummary)
                    }
                    return true
                } else if(preference.key.equals(getString(R.string.key_active_preference))){
                    val service = Intent(requireContext(),PeriodicalNotificationService::class.java)
                    if(newValue as Boolean){
                        service.putExtra(PeriodicalNotificationService.EXTRA_ACTION,ACTION_START)
                    } else{
                        service.putExtra(PeriodicalNotificationService.EXTRA_ACTION,ACTION_STOP)
                    }
                    requireContext().startService(service)
                    return true
                } else {
                    Log.e(TAG,"Preference is null")
                }
            }
            return false
        }

        private fun bindPreferenceSummaryToValue(preference: Preference?) {
            if (preference != null){
                preference.setOnPreferenceChangeListener(this)
                val notifPref = NotificationPreference(requireContext())
                if (preference.key.equals(notifPref.KEY_ACTIVE)){
                    onPreferenceChange(preference,notifPref.isActive())
                } else if (preference.key.equals(notifPref.KEY_MINUTES)){
                    onPreferenceChange(preference, notifPref.getNotifMinutes())
                }
            } else{
                Log.e(TAG,"Preference is null")
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}