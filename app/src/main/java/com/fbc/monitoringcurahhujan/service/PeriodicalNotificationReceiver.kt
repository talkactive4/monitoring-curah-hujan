package com.fbc.monitoringcurahhujan.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.fbc.monitoringcurahhujan.source.preference.NotificationPreference
import com.fbc.monitoringcurahhujan.source.preference.NotificationSettingsPreference
import com.fbc.monitoringcurahhujan.source.worker.CreateNotificationWorker
import java.util.*

class PeriodicalNotificationReceiver : BroadcastReceiver() {

    companion object{
        const val PERIODICAL_NOTIFICATION_ID = 1
        const val PERIODICAL_NOTIFICATION_CHANNEL_ID = "periodical_notification_channel"
        const val PERIODICAL_NOTIFICATION_CHANNEL_NAME = "Periodical Notification Channel"
    }

    private var mContext: Context? = null
    
    override fun onReceive(context: Context, intent: Intent) {
        mContext = context

        val notifPref = NotificationPreference(context)
        val settingsPref = NotificationSettingsPreference(context)

        val nextNotifyTime = Calendar.getInstance()
        nextNotifyTime.add(Calendar.MINUTE, Integer.parseInt(notifPref.getNotifMinutes()))

        // preference supaya ketika device sudah di boot, bisa ambil schedule di sini
        settingsPref.setScheduled(nextNotifyTime.timeInMillis)
        settingsPref.setRunning(false)

//        retrieveData(context)

        // get data when internet is available
        val workManager = WorkManager.getInstance(context)
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED).build()
        val work = OneTimeWorkRequest.Builder(CreateNotificationWorker::class.java)
            .setConstraints(constraints).build()
        workManager.enqueue(work)

        // reschedule
        val i = Intent(context, PeriodicalNotificationService::class.java)
        i.putExtra(PeriodicalNotificationService.EXTRA_ACTION, PeriodicalNotificationService.ACTION_START)
        context.startService(i)
    }
}
