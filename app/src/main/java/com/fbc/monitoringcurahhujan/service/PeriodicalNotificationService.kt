package com.fbc.monitoringcurahhujan.service

import android.app.AlarmManager
import android.app.IntentService
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import com.fbc.monitoringcurahhujan.source.preference.NotificationPreference
import com.fbc.monitoringcurahhujan.source.preference.NotificationSettingsPreference
import java.util.*

class PeriodicalNotificationService : IntentService("PeriodicalNotificationService") {

    companion object{
        const val PERIODICAL_NOTIFICIATION_REQUEST_CODE = 100
        const val EXTRA_ACTION = "periodical_notification_action"
        const val ACTION_START = 1
        const val ACTION_STOP = 0
    }


    private lateinit var notifPref:NotificationPreference
    private lateinit var settingsPref:NotificationSettingsPreference
    private var notificationIntent: Intent? = null
    private var pendingIntent: PendingIntent? = null
    private var manager: AlarmManager? = null

    override fun onHandleIntent(intent: Intent?) {
        notifPref = NotificationPreference(this)
        settingsPref = NotificationSettingsPreference(this)
        val action = intent!!.getIntExtra(EXTRA_ACTION, 0)
        setUp(this)
        if(notifPref.isActive() || action == ACTION_START){
            startNotification(this)
        } else {
            stopNotification(this)
        }

    }

    private fun setUp(context: Context) {
        if (notificationIntent == null) notificationIntent = Intent(context, PeriodicalNotificationReceiver::class.java)
        if (pendingIntent == null) {
            pendingIntent = PendingIntent.getBroadcast(
                context,
                PERIODICAL_NOTIFICIATION_REQUEST_CODE,
                notificationIntent,
                0
            )
        }
        if (manager == null) manager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
    }

    private fun startNotification(context: Context){
        Log.i(context.toString(), "Starting Intervals")

        // Pasang interval
        var calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()

        // jika alarm manager ternyata sudah di schedule tetapi tidak di receive oleh
        // broadcast receiver set alarm manager untuk direcieve 1 menit kedepan,
        // jika tidak, set alarm manager untuk 1 jam
        if (settingsPref.isRunning()) {
            val scheduledTime: Calendar = GregorianCalendar()
            scheduledTime.timeInMillis = settingsPref.getScheduledTime()
            if (calendar.after(scheduledTime)) {
                calendar.add(Calendar.MINUTE, 1) // MINUTE
            } else {
                calendar = scheduledTime
            }
        } else {
            calendar.add(Calendar.MINUTE, Integer.parseInt(notifPref.getNotifMinutes()))
        }

        // preference supaya ketika device sudah di boot, bisa ambil schedule di sini
        settingsPref.setScheduled(calendar.timeInMillis)
        settingsPref.setRunning(true)

        // jika manager tidak null, pasang interval
        if (manager != null) {
            manager!!.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)

            // Untuk sdk device diatas android M
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                manager!!.setExactAndAllowWhileIdle(
                    AlarmManager.RTC_WAKEUP, calendar.timeInMillis,
                    pendingIntent
                )
            }
        }
    }

    private fun stopNotification(context: Context){
        // jika pendingIntent ada dan manager tidak kosong, cancel pendingIntent
        if (PendingIntent.getBroadcast(
                context,
                PERIODICAL_NOTIFICIATION_REQUEST_CODE,
                notificationIntent,
                0
            ) != null
            && manager != null
        ) {
            Log.i(context.toString(), "Stopping Intervals")
            manager!!.cancel(pendingIntent)
            settingsPref.setRunning(false)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(this.toString(), "Periodical Notification Service is destroyed")
    }

}
