package com.fbc.monitoringcurahhujan.data

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CurahHujan(
    var statusHujan:String = "Data Kosong",
    var curahHujan:Double = 0.0,
    var waktu:Timestamp = Timestamp.now()
): Parcelable