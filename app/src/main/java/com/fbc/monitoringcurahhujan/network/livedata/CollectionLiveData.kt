package com.fbc.monitoringcurahhujan.network.livedata

import android.util.Log
import androidx.lifecycle.LiveData
import com.google.firebase.firestore.*

class CollectionLiveData : LiveData<QuerySnapshot> {

    private val query: Query

    private val eventListener = CustomEventListener()

    constructor(reference: CollectionReference) {
        query = reference
    }

    constructor(query: Query) {
        this.query = query
    }

    override fun onActive() {
        query.addSnapshotListener(eventListener)
    }

    private inner class CustomEventListener : EventListener<QuerySnapshot> {
        override fun onEvent(
            querySnapshot: QuerySnapshot?,
            exception: FirebaseFirestoreException?
        ) {
            when (exception) {
                null -> {
                    querySnapshot?.let { snapshot ->
                        value = snapshot
                    }
                }
                else -> {
                    Log.e("LiveData", "${exception.message}")
                }
            }
        }
    }

}