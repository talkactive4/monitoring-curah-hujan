package com.wasdlabs.esenat.network

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot

class DocumentLiveData(private val reference: DocumentReference) : LiveData<DocumentSnapshot>() {

    override fun onActive() {
        super.onActive()
        reference.get().addOnSuccessListener {
            it?.let { documentSnapshot ->
                value = documentSnapshot
            }
        }
    }

}