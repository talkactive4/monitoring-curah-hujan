package com.fbc.monitoringcurahhujan.network.repository

import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.fbc.monitoringcurahhujan.network.livedata.CollectionLiveData
import com.wasdlabs.esenat.network.DocumentLiveData

/**
 * Sebuah abstract class yang dapat digunakan sebagai
 * template untuk semua repository
 * */
open class FirestoreRepository<T : Any>(path: String) {

    protected val reference = Firebase.firestore.collection(path)

    val collectionLiveData =
        CollectionLiveData(reference)

    fun create(value: T) {
        reference.add(value)
    }

    fun delete(key: String) {
        reference.document(key).delete()
    }

    fun update(key: String, value: T) {
        reference.document(key).set(value)
    }

    fun getDocumentByKey(key: String): DocumentLiveData {
        return DocumentLiveData(
            reference.document(
                key
            )
        )
    }
}