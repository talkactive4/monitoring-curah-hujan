package com.fbc.monitoringcurahhujan.network.repository

import com.fbc.monitoringcurahhujan.data.CurahHujan
import com.fbc.monitoringcurahhujan.network.livedata.CollectionLiveData
import com.google.firebase.Timestamp
import com.google.firebase.firestore.Query
import java.util.*

class CurahHujanRepository : FirestoreRepository<CurahHujan>("rainfall"){

    companion object{
        const val FIELD_CURAH_HUJAN = "curahHujan"
        const val FIELD_STATUS_HUJAN = "statusHujan"
        const val FIELD_WAKTU = "waktu"
    }

    val latestRainfall = CollectionLiveData(reference.orderBy(FIELD_WAKTU, Query.Direction.DESCENDING).limit(1))
    val latestRainfalls = CollectionLiveData(reference.orderBy(FIELD_WAKTU, Query.Direction.DESCENDING).limit(10))
    val latestRainfallQuery = reference.orderBy(FIELD_WAKTU, Query.Direction.DESCENDING).limit(1)

    fun getRainfall():Query{
        return reference.orderBy(FIELD_WAKTU, Query.Direction.DESCENDING).limit(10)
    }

    fun getRainfallBefore(date: Date?): Query {
        var queryDate = date
        if (queryDate == null) queryDate = Date()
        val timestamp: Timestamp = Timestamp(queryDate)
        return reference.whereLessThanOrEqualTo(FIELD_WAKTU,timestamp)
            .orderBy(FIELD_WAKTU, Query.Direction.DESCENDING)
            .limit(10)
    }

    fun getRainfallAfter(date: Date?): Query {
        var queryDate = date ?: Date()
        val timestamp: Timestamp = Timestamp(queryDate)
        return reference.whereGreaterThanOrEqualTo(FIELD_WAKTU,timestamp)
            .orderBy(FIELD_WAKTU, Query.Direction.DESCENDING)
            .limit(10)
    }

    fun getRainfallBetween(from:Date?,to:Date?):Query{
        var queryFrom = from ?: Date()
        var queryTo = to ?: Date()
        return reference.whereGreaterThan(FIELD_WAKTU,queryFrom)
            .whereLessThanOrEqualTo(FIELD_WAKTU,queryTo)
            .orderBy(FIELD_WAKTU, Query.Direction.DESCENDING)
            .limit(20)

    }

}