package com.fbc.monitoringcurahhujan.source.worker

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.fbc.monitoringcurahhujan.R
import com.fbc.monitoringcurahhujan.data.CurahHujan
import com.fbc.monitoringcurahhujan.network.repository.CurahHujanRepository
import com.fbc.monitoringcurahhujan.service.PeriodicalNotificationReceiver
import com.fbc.monitoringcurahhujan.service.PeriodicalNotificationService

class CreateNotificationWorker(val context: Context,params:WorkerParameters): Worker(context,params) {

    override fun doWork(): Result {
        retrieveData(context)
        return Result.success()
    }


    private fun retrieveData(context: Context){
        var data: CurahHujan? = null
        val repo = CurahHujanRepository()
        repo.latestRainfallQuery.addSnapshotListener { snapshot, firebaseFirestoreException ->
            snapshot?.let{
                for(item in snapshot){
                    data = item.toObject(CurahHujan::class.java)
                }
                data?.let { createNotification(context, it) }
            }
        }
    }

    private fun createNotification(context: Context, curahHujan: CurahHujan){
        val reminderIntent = Intent(context, PeriodicalNotificationService::class.java)
        reminderIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

        val pendingIntent = PendingIntent.getActivity(
            context, PeriodicalNotificationService.PERIODICAL_NOTIFICIATION_REQUEST_CODE,
            reminderIntent, 0
        )

        val notificationBuilder: NotificationCompat.Builder
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                PeriodicalNotificationReceiver.PERIODICAL_NOTIFICATION_CHANNEL_ID,
                PeriodicalNotificationReceiver.PERIODICAL_NOTIFICATION_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.description =
                PeriodicalNotificationReceiver.PERIODICAL_NOTIFICATION_CHANNEL_NAME
            notificationManager.createNotificationChannel(channel)
        }

        notificationBuilder = NotificationCompat.Builder(
            context,
            PeriodicalNotificationReceiver.PERIODICAL_NOTIFICATION_CHANNEL_ID
        )
        notificationBuilder.setAutoCancel(true)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setWhen(System.currentTimeMillis())
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setTicker("{" + "Lihat Data Terbaru" + "}")
            .setContentTitle("Data Curah Hujan Terbaru")
            .setContentText("Curah hujan: "+curahHujan.curahHujan +" mm")
            .setContentInfo("INFO")
            .setContentIntent(pendingIntent)

        if (notificationManager != null) {
            notificationManager.notify(
                PeriodicalNotificationReceiver.PERIODICAL_NOTIFICATION_ID,
                notificationBuilder.build()
            )
        }
    }

}