package com.fbc.monitoringcurahhujan.source.preference

import android.content.Context
import android.content.SharedPreferences
import com.fbc.monitoringcurahhujan.R

class NotificationSettingsPreference(private val context: Context) {

    val PREF_NAME = "NotificationSettingsPreference"
    val KEY_RUNNING = context.getString(R.string.key_running_preference)
    val RUNNING_DEFAULT:Boolean = false
    val KEY_SCHEDULED = context.getString(R.string.key_scheduled_preference)
    val SCHEDULED_DEFAULT:Long = java.util.Calendar.getInstance().getTimeInMillis()

    private val sharedPref: SharedPreferences = context.getSharedPreferences(PREF_NAME,
        Context.MODE_PRIVATE)

    fun isRunning():Boolean{
        return sharedPref.getBoolean(KEY_RUNNING,RUNNING_DEFAULT)
    }
    fun getScheduledTime():Long{
        return sharedPref.getLong(KEY_SCHEDULED,SCHEDULED_DEFAULT)
    }

    fun setRunning(isRunning:Boolean){
        sharedPref.edit()
            .putBoolean(KEY_RUNNING,isRunning)
            .apply()
    }
    fun setScheduled(millis:Long){
        sharedPref.edit()
            .putLong(KEY_SCHEDULED,millis)
            .apply()
    }
}