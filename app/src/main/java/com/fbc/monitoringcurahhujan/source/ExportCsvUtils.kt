package com.fbc.monitoringcurahhujan.source

import android.content.ContentValues
import android.content.Context
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import com.fbc.monitoringcurahhujan.data.CurahHujan
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*

class ExportCsvUtils(
    private val context: Context,
    private val onExportCompleteListener: OnExportCompleteListener) {

    fun startExport(rainfallData:List<Pair<String,CurahHujan>>,filename:String){
        var outputStream: OutputStream? = null
        try {
            val csvFile:File
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                val resolver = context.contentResolver
                val contentValues = ContentValues()
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, filename+".csv")
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "text/csv")
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH,Environment.DIRECTORY_DOCUMENTS)
                val fileUri =resolver.insert(MediaStore.Files.getContentUri("external"), contentValues)
                fileUri?.let{
                    outputStream = resolver.openOutputStream(it)
                }

            } else {
                val dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString()
                csvFile = File(dir,filename+".csv")
                outputStream = FileOutputStream(csvFile)
            }

            val colString = "\"Curah Hujan\",\"Status\",\"Tanggal/Waktu\""
            var dataString:String = ""
            val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
            for(data in rainfallData){
                val curahHujan = data.second.curahHujan.toString() + ","
                val status = data.second.statusHujan.toString() + ","
                val waktu = sdf.format(data.second.waktu.toDate()) + "\n"
                dataString += curahHujan+status+waktu
            }
            val combinedString = colString + "\n" + dataString
            outputStream?.let {
                it.write(combinedString.toByteArray(Charsets.UTF_8))
                it.close()
                onExportCompleteListener.onDone(true,"Export complete!")
                return
            }
            onExportCompleteListener.onDone(false,"Can't export file!")
        } catch (e:Exception){
            e.printStackTrace()
            onExportCompleteListener.onDone(false,e.message.toString())
        }
    }

    class OnExportCompleteListener(private val clickListener: (Boolean,String) -> Unit) {
        fun onDone(isCompleted: Boolean, message: String) = clickListener(isCompleted,message)
    }

}