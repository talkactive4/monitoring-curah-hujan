package com.fbc.monitoringcurahhujan.source.preference

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.fbc.monitoringcurahhujan.R

class NotificationPreference(private val context: Context) {

//    val PREF_NAME = "NotificationPreference"
    val KEY_ACTIVE = context.getString(R.string.key_active_preference)
    val ACTIVE_DEFAULT:Boolean = false
    val KEY_MINUTES = context.getString(R.string.key_minutes_preference)
    val MINUTES_DEFAULT:String = "15"

    private val sharedPref: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun isActive():Boolean{
        return sharedPref.getBoolean(KEY_ACTIVE,ACTIVE_DEFAULT)
    }
    fun getNotifMinutes():String{
        return sharedPref.getString(KEY_MINUTES,MINUTES_DEFAULT)?: MINUTES_DEFAULT
    }


    fun setActive(isActive:Boolean){
        sharedPref.edit()
            .putBoolean(KEY_ACTIVE,isActive)
            .apply()
    }
    fun setNotifMinutes(minutes:String){
        sharedPref.edit()
            .putString(KEY_MINUTES,minutes)
            .apply()
    }

}